import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import Landing from './pages/Landing';
import StickyNav from './components/StickyNav';
import Footer from './components/Footer';

function App() {
  return (
    <div>
      <BrowserRouter>
      <StickyNav/>
        <Routes>
          <Route path='/' element={<Landing />} />
           
        </Routes>
        <Footer/>
      </BrowserRouter>
    </div>
  );
}

export default App;
