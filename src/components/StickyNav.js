import React from "react";
import { render } from "react-dom";
import "./stickynav.css";
import logo  from '../assets/logo.png';
import { Container } from "react-bootstrap";
import { Navbar,NavDropdown,Nav,Row,Col } from 'react-bootstrap'
import {FaCaretDown, FaMapMarkerAlt,FaBullhorn,FaTelegramPlane,FaSearch } from 'react-icons/fa';
import { Link, NavLink } from "react-router-dom";

class StickyNav extends React.Component {
  constructor(props) {
    super(props);
    this.state = { height: 0, scrolltop: 0 };
    this.handleScroll = this.handleScroll.bind(this);
  }

  componentDidMount() {
    const height = this.divElement.clientHeight;

    this.setState({ height });
    window.addEventListener("scroll", this.handleScroll);
  }
  componentDidUpdate(prevProps, prevState) {
    const scrollTop2 = window.pageYOffset;

    if (scrollTop2 !== prevState.scrolltop)
    {
      this.setState({ scrolltop: scrollTop2 });

    }
  }
 
  handleScroll(event) {
    const scrollTop = window.scrollTo;

    this.setState({
      scrolltop: scrollTop
    });
  }
  render() {
    return (
      <div >
       
        <header ref={divElement => (this.divElement = divElement)}>
        <Container>
        <div className="header-fix">
          <div className="logo-left">
            {/* Logo */}
            <div className="logo">
            <img src={logo} alt="" />
            </div>
                <div className="brand">
                <h1>Cosco Computer</h1>
                </div>
        
          </div>
          <div className="header-right">
            {/* right content */}
            <div className="top-content">
              {/* top */}
           
              <div className="top-feed">
                <div className="icon ">
               <FaBullhorn className="ic rounded-circle"/>
                </div>
                <div className="txt">
                 <h2>Telegram Channel</h2>
                 <p>@Cosco computer</p>
                </div>
              </div>
              <div className="top-feed">
                <div className="icon ">
               <FaTelegramPlane className="ic rounded-circle"/>
                </div>
                <div className="txt">
                 <h2>Contact Us</h2>
                 <p>+855 16 595 168 </p>
                 <p>+855 87 87 55 66</p>
                </div>
              </div>
              <div className="top-feed">
                <div className="icon ">
               <FaMapMarkerAlt className="ic rounded-circle"/>
                </div>
                <div className="txt">
                 <h2>Address</h2>
                 <p>No 420, St.128 Sangkat Monorom
Khan 7Makara, Phnom Penh</p>
                </div>
              </div>

            </div>
            <div className="buttom-content">
              {/* buttom
              <FontAwesomeIcon icon={faCoffee} /> */}
              
            {/* <label for="search" class="fas fa-search"> */}
            <form>
   
            <select
                  
                    className=""
                    name="option"
                    id="option"
                  >
                   <option className="myOP">Brand</option>
                   <option >MSI</option>
                   <option >Lenovo</option>
                   <option >Dell</option>
                  </select>
        <input type="text" id="search" placeholder="Search all product here"/>
        <label for="search"><FaSearch className="searchI"/></label>
    
    </form>
            </div>
          </div>

      </div>
      </Container>
          {/* <h1>This is a Sticky Nav Demo!</h1>
          <p>
            Creating one of these isn't so bad. Let's learn how with this sweet
            little demo!
          </p> */}
        </header>
       
        {/* <nav
          className={
            this.state.scrolltop > this.state.height
              ? "main-nav main-nav-scrolled"
              : "main-nav"
          }
        >
          <a href="#">Nav Link 1</a>
          <a href="#">Nav Link 2</a>
          <a href="#">Nav Link 3</a>
          <a href="#">Nav Link 4 test </a>
        </nav> */}

        {/* got */}
        <Navbar  className={
            this.state.scrolltop > this.state.height
              ? "main-nav main-nav-scrolled"
              : "main-nav"
          } bg="light" expand="lg">
      <Container>
        {/* <Navbar.Brand href="#home"></Navbar.Brand> */}
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="me-auto">
            
            {/* <Nav.Link href="#home">Home</Nav.Link>
            <Nav.Link href="#link">Link</Nav.Link> */}
        
            <NavLink className="sub-menu" as={Link} to="/">New Arrivall</NavLink>
            <NavLink className="sub-menu" as={Link} to="/"> 
            <div class="dropdown show">
                                   <NavLink className="sub-menu" as={Link} to="/">Laptop <FaCaretDown/></NavLink>
                          <div class="dropdown-menus" aria-labelledby="dropdownMenuLink">
                          <NavLink className="dropdown-item sna" as={Link} to="/">Lenovo</NavLink>
                          <NavLink className="dropdown-item sna" as={Link} to="/">Acer</NavLink>
                          <NavLink className="dropdown-item sna" as={Link} to="/">Asus</NavLink>
                          <NavLink className="dropdown-item sna" as={Link} to="/">MSI</NavLink>
                          <NavLink className="dropdown-item sna" as={Link} to="/">Apple</NavLink>
                          <NavLink className="dropdown-item sna" as={Link} to="/">HP</NavLink>
                          <NavLink className="dropdown-item sna" as={Link} to="/">Dell</NavLink>
                          </div>
            </div></NavLink>
            <NavLink className="sub-menu" as={Link} to="/"> 
            <div class="dropdown show">
                                   <NavLink className="sub-menu" as={Link} to="/">Used Desktop <FaCaretDown/></NavLink>
                          <div class="dropdown-menus" aria-labelledby="dropdownMenuLink">
                          <NavLink className="dropdown-item sna" as={Link} to="/">Lenovo</NavLink>
                          <NavLink className="dropdown-item sna" as={Link} to="/">Acer</NavLink>
                          <NavLink className="dropdown-item sna" as={Link} to="/">Asus</NavLink>
                          <NavLink className="dropdown-item sna" as={Link} to="/">MSI</NavLink>
                          <NavLink className="dropdown-item sna" as={Link} to="/">Apple</NavLink>
                          <NavLink className="dropdown-item sna" as={Link} to="/">HP</NavLink>
                          <NavLink className="dropdown-item sna" as={Link} to="/">Dell</NavLink>
                          </div>
            </div></NavLink>
        
            <NavLink className="sub-menu" as={Link} to="/"> 
            <div class="dropdown show">
                                   <NavLink className="sub-menu" as={Link} to="/">Desktop <FaCaretDown/></NavLink>
                          <div class="dropdown-menus" aria-labelledby="dropdownMenuLink">
                          <NavLink className="dropdown-item sna" as={Link} to="/">Lenovo</NavLink>
                          <NavLink className="dropdown-item sna" as={Link} to="/">Acer</NavLink>
                          <NavLink className="dropdown-item sna" as={Link} to="/">Asus</NavLink>
                          <NavLink className="dropdown-item sna" as={Link} to="/">MSI</NavLink>
                          <NavLink className="dropdown-item sna" as={Link} to="/">Apple</NavLink>
                          <NavLink className="dropdown-item sna" as={Link} to="/">HP</NavLink>
                          <NavLink className="dropdown-item sna" as={Link} to="/">Dell</NavLink>
                          </div>
            </div></NavLink>
            <NavLink className="sub-menu" as={Link} to="/"> 
            <div class="dropdown show">
                                   <NavLink className="sub-menu" as={Link} to="/">Used Laptop <FaCaretDown/></NavLink>
                          <div class="dropdown-menus" aria-labelledby="dropdownMenuLink">
                          <NavLink className="dropdown-item sna" as={Link} to="/">Lenovo</NavLink>
                          <NavLink className="dropdown-item sna" as={Link} to="/">Acer</NavLink>
                          <NavLink className="dropdown-item sna" as={Link} to="/">Asus</NavLink>
                          <NavLink className="dropdown-item sna" as={Link} to="/">MSI</NavLink>
                          <NavLink className="dropdown-item sna" as={Link} to="/">Apple</NavLink>
                          <NavLink className="dropdown-item sna" as={Link} to="/">HP</NavLink>
                          <NavLink className="dropdown-item sna" as={Link} to="/">Dell</NavLink>
                          </div>
            </div></NavLink>
            <NavLink className="sub-menu" as={Link} to="/"> 
            <div class="dropdown show">
                                   <NavLink className="sub-menu" as={Link} to="/">Used Desktop <FaCaretDown/></NavLink>
                          <div class="dropdown-menus" aria-labelledby="dropdownMenuLink">
                          <NavLink className="dropdown-item sna" as={Link} to="/">Lenovo</NavLink>
                          <NavLink className="dropdown-item sna" as={Link} to="/">Acer</NavLink>
                          <NavLink className="dropdown-item sna" as={Link} to="/">Asus</NavLink>
                          <NavLink className="dropdown-item sna" as={Link} to="/">MSI</NavLink>
                          <NavLink className="dropdown-item sna" as={Link} to="/">Apple</NavLink>
                          <NavLink className="dropdown-item sna" as={Link} to="/">HP</NavLink>
                          <NavLink className="dropdown-item sna" as={Link} to="/">Dell</NavLink>
                          </div>
            </div></NavLink>
            <NavLink className="sub-menu" as={Link} to="/"> 
            <div class="dropdown show">
                                   <NavLink className="sub-menu" as={Link} to="/">Accessories <FaCaretDown/></NavLink>
                          <div class="dropdown-menus" aria-labelledby="dropdownMenuLink">
                          <NavLink className="dropdown-item sna" as={Link} to="/">Lenovo</NavLink>
                          <NavLink className="dropdown-item sna" as={Link} to="/">Acer</NavLink>
                          <NavLink className="dropdown-item sna" as={Link} to="/">Asus</NavLink>
                          <NavLink className="dropdown-item sna" as={Link} to="/">MSI</NavLink>
                          <NavLink className="dropdown-item sna" as={Link} to="/">Apple</NavLink>
                          <NavLink className="dropdown-item sna" as={Link} to="/">HP</NavLink>
                          <NavLink className="dropdown-item sna" as={Link} to="/">Dell</NavLink>
                          </div>
            </div></NavLink>
            <NavLink className="sub-menu" as={Link} to="/">Smartphone</NavLink>
            <NavLink className="sub-menu" as={Link} to="/">Contact Us</NavLink>



            {/* <NavDropdown title="Dropdown" id="basic-nav-dropdown">
              <NavDropdown.Item href="#action/3.1">Action</NavDropdown.Item>
              <NavDropdown.Item href="#action/3.2">
                Another action
              </NavDropdown.Item>
              <NavDropdown.Item href="#action/3.3">Something</NavDropdown.Item>
              <NavDropdown.Divider />
              <NavDropdown.Item href="#action/3.4">
                Separated link
              </NavDropdown.Item>
            </NavDropdown> */}
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
    
    {/*  */}
    
      
        {/* <div className="main">
          <h2>Lorem ipsum dolor sit amet</h2>
          <p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean
            fringilla, purus in porttitor pellentesque, erat arcu tincidunt
            diam, dapibus faucibus leo mauris at sapien. In porttitor vehicula
            sodales. Vivamus massa neque, facilisis eu felis ut, aliquet
            convallis nisi. Nam elementum tellus vitae gravida fermentum. Nullam
            et imperdiet leo. Integer ut euismod lorem, in placerat lacus.
            Curabitur bibendum arcu ut feugiat commodo. Suspendisse ut mi vel
            orci ullamcorper tincidunt. Nam vitae fringilla nibh. Nullam
            hendrerit blandit velit eu hendrerit.
          </p>

          <p>
            Praesent eu enim non massa pellentesque lobortis. In in sagittis
            dolor. Aliquam non massa erat. Ut aliquet gravida tellus, sed
            volutpat nibh condimentum et. Nunc quam purus, vehicula quis
            venenatis et, porttitor vel dolor. Cras facilisis dui id elit
            bibendum, in ullamcorper leo ultricies. Praesent rutrum lacus sit
            amet sem convallis, ut interdum dolor vestibulum.
          </p>

          <p>
            Tum sociis natoque penatibus et magnis dis parturient montes,
            nascetur ridiculus mus. In iaculis pharetra odio, sit amet
            consectetur elit facilisis ac. Praesent eget tristique ipsum. Donec
            malesuada bibendum lacinia. Praesent non purus sodales, pulvinar mi
            vitae, tincidunt leo. Phasellus vitae elit ut nisl semper fringilla
            id rutrum dolor. Donec a massa adipiscing, cursus risus vitae,
            porttitor tortor. Nullam sagittis est sapien, sit amet pharetra
            turpis imperdiet vel. Etiam sit amet ligula pretium, vulputate eros
            ac, bibendum velit. Aenean convallis ante purus, ac bibendum orci
            laoreet ac. Donec a convallis mauris. Nulla non lacus non ipsum
            pretium tempor. In hac habitasse platea dictumst. Aenean at ipsum
            vulputate, sagittis dui ut, pharetra neque. Nam eget sodales orci.
            Aliquam pharetra nunc at nisl pellentesque, nec fringilla enim
            iaculis.
          </p>
          <p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean
            fringilla, purus in porttitor pellentesque, erat arcu tincidunt
            diam, dapibus faucibus leo mauris at sapien. In porttitor vehicula
            sodales. Vivamus massa neque, facilisis eu felis ut, aliquet
            convallis nisi. Nam elementum tellus vitae gravida fermentum. Nullam
            et imperdiet leo. Integer ut euismod lorem, in placerat lacus.
            Curabitur bibendum arcu ut feugiat commodo. Suspendisse ut mi vel
            orci ullamcorper tincidunt. Nam vitae fringilla nibh. Nullam
            hendrerit blandit velit eu hendrerit.
          </p>

          <p>
            Praesent eu enim non massa pellentesque lobortis. In in sagittis
            dolor. Aliquam non massa erat. Ut aliquet gravida tellus, sed
            volutpat nibh condimentum et. Nunc quam purus, vehicula quis
            venenatis et, porttitor vel dolor. Cras facilisis dui id elit
            bibendum, in ullamcorper leo ultricies. Praesent rutrum lacus sit
            amet sem convallis, ut interdum dolor vestibulum.
          </p>

          <p>
            Tum sociis natoque penatibus et magnis dis parturient montes,
            nascetur ridiculus mus. In iaculis pharetra odio, sit amet
            consectetur elit facilisis ac. Praesent eget tristique ipsum. Donec
            malesuada bibendum lacinia. Praesent non purus sodales, pulvinar mi
            vitae, tincidunt leo. Phasellus vitae elit ut nisl semper fringilla
            id rutrum dolor. Donec a massa adipiscing, cursus risus vitae,
            porttitor tortor. Nullam sagittis est sapien, sit amet pharetra
            turpis imperdiet vel. Etiam sit amet ligula pretium, vulputate eros
            ac, bibendum velit. Aenean convallis ante purus, ac bibendum orci
            laoreet ac. Donec a convallis mauris. Nulla non lacus non ipsum
            pretium tempor. In hac habitasse platea dictumst. Aenean at ipsum
            vulputate, sagittis dui ut, pharetra neque. Nam eget sodales orci.
            Aliquam pharetra nunc at nisl pellentesque, nec fringilla enim
            iaculis.
          </p>
        </div> */}
      </div>
    );
  }
}
export default StickyNav;
