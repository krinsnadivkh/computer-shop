import React from 'react'
import { Col, Container, Row } from 'react-bootstrap'
import { Link, NavLink } from 'react-router-dom';
import "./stickynav.css";
import {FaCaretDown, FaMapMarkerAlt,FaBullhorn,FaTelegramPlane,FaSearch, FaAngleRight, FaRegEnvelope } from 'react-icons/fa';

export default function Footer() {
  return (
    <div>
        <Row>
            <Col className="content-footer" lg={12}>
                <Container>
                <Row>
                    <Col className="quick-menu" xl={4} lg={4} md={4} sm={12}>
                        <h2 className="head">Quick Menu</h2>
                        <ul>
                            <li><NavLink className="q-memu" as={Link} to="/"><FaAngleRight/> Lenovo</NavLink></li>
                            <li> <NavLink className="q-menu" as={Link} to="/"><FaAngleRight/> Acer</NavLink></li>
                            <li> <NavLink className="q-menu" as={Link} to="/"><FaAngleRight/> Asus</NavLink></li>
                            <li>  <NavLink className="q-menu" as={Link} to="/"><FaAngleRight/> MSI</NavLink></li>
                            <li> <NavLink className="q-menu" as={Link} to="/"><FaAngleRight/> Apple</NavLink></li>
                            <li> <NavLink className="q-menu" as={Link} to="/"><FaAngleRight/> HP</NavLink></li>
                            <li> <NavLink className="q-menu" as={Link} to="/"><FaAngleRight/> Dell</NavLink></li>
                        </ul>


                    </Col>
                    <Col className="quick-menu" xl={4} lg={4} md={4} sm={12}>
                    <h2 className="head">About Us</h2>
                    <ul>
                    <li>
                                      <div className="about-card">
                                          <div className="icon ">
                                              <FaMapMarkerAlt className="ic rounded-circle" />
                                          </div>
                                          <div className="txt">
                                           
                                              <p> #420, St.128 Sangkat Monorom
Khan 7Makara, Phnom Penh</p>
                                          </div>
                                      </div>
                        </li>
                        <li>
                                      <div className="about-card">
                                          <div className="icon ">
                                              <FaTelegramPlane className="ic rounded-circle" />
                                          </div>
                                          <div className="txt">
                                           
                                              <p>@Cosco computer</p>
                                          </div>
                                      </div>
                        </li>
                        <li>
                                      <div className="about-card">
                                          <div className="icon ">
                                              <FaRegEnvelope className="ic rounded-circle" />
                                          </div>
                                          <div className="txt">
                                           
                                              <p>costscogroup@gmail.com
</p>
                                          </div>
                                      </div>
                        </li>
                    </ul>

                    </Col>
                    <Col className="quick-menu" xl={4} lg={4} md={4} sm={12}>
                    <h2 className="head">Socail</h2>
                    <div class="fb-page" data-href="https://web.facebook.com/costscocomputer/" data-tabs="cover" data-width="380" data-height="160" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://web.facebook.com/costscocomputer/" class="fb-xfbml-parse-ignore"><a href="https://web.facebook.com/costscocomputer/">Costsco Group</a></blockquote></div>
            
                    </Col>
                </Row>
                </Container>

            </Col>
            <Col className="copy-right" lg={12}>
                <p>@2022 Costsco All Right Reserved | Designed by: <span>    <NavLink as={Link} to="/">Developer Team</NavLink></span> </p>
            </Col>
        </Row>


    </div>
  )
}
